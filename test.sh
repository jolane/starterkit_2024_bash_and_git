#!/usr/bin/env bash


j=0
for i in {0..10};
do
    echo $i
    (( j++ ))
done

echo $j
a=$(( j + 1 ))
echo $a

if [ -f $HOME/.zshrc ]
then
    echo Have .zshrc in $HOME .zshrc
else
    echo No .zshrc in $HOME
fi
